﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController2 : MonoBehaviour
{
    public float forceMult = 0.5f;
    private Rigidbody2D rb;

    [Space(20)]
    [Range(1, 20)]
    public float jumpVelocity;
    public bool Jump;
    public LayerMask groundLayer;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    void Awake()
    {
        rb = GetComponent<Rigidbody2D>();
    }
    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.A))
        {
            //rb.velocity = new Vector2(-forceMult, 0);
            rb.AddForce(new Vector2(-forceMult, 0), ForceMode2D.Impulse);
            //transform.Translate(Vector3.left * MoveSpeed * Time.deltaTime, Space.World);
        }

        if (Input.GetKey(KeyCode.D))
        {
            //rb.velocity = new Vector2(forceMult, 0);
            rb.AddForce(new Vector2(forceMult, 0), ForceMode2D.Impulse);
            //transform.Translate(Vector3.right * MoveSpeed * Time.deltaTime, Space.World);
        }

        if (IsGrounded() == true)
        {
            Jump = true;
        }
        else
        {
            Jump = false;
        }


    }
    private void Update()
    {
        if (FindObjectOfType<PlayerController2>().Jump)
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                FindObjectOfType<AudioManager>().PlaySound("Jump");
                //GetComponent<Rigidbody2D>().velocity = Vector2.up * jumpVelocity;
                GetComponent<Rigidbody2D>().AddForce((Vector2.up * jumpVelocity), ForceMode2D.Impulse);
            }
        }

        if (rb.velocity.y < 0)
        {
           rb.velocity += Vector2.up * Physics2D.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        }
        else if (rb.velocity.y > 0 && !Input.GetButton("Jump") && !Input.GetKeyDown(KeyCode.W))
        {
           rb.velocity += Vector2.up * Physics2D.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }
    bool IsGrounded()
    {
        Vector2 position = new Vector2(transform.position.x, transform.position.y);
        Vector2 position1 = new Vector2(transform.position.x + 0.10f, transform.position.y);
        Vector2 position2 = new Vector2(transform.position.x - 0.10f, transform.position.y);
        Vector2 directionDown = Vector2.down;
        Vector2 directionLeft = Vector2.left;
        Vector2 directionRight = Vector2.right;
        float distance = 0.7f;
        RaycastHit2D hit = Physics2D.Raycast(position1, directionDown, distance, groundLayer);
        RaycastHit2D hit1 = Physics2D.Raycast(position2, directionDown, distance, groundLayer);
        RaycastHit2D hitLeft = Physics2D.Raycast(position, directionLeft, distance, groundLayer);
        RaycastHit2D hitRight = Physics2D.Raycast(position, directionRight, distance, groundLayer);
        if ((hitLeft.collider != null || hitRight.collider != null) && (hit.collider == null || hit1.collider == null))
        {
            //Debug.Log("false");
            return false;
        }
        if (hit.collider != null || hit1.collider != null)
        {
            //Debug.Log("true");
            return true;
        }
        //Debug.Log("false");
        return false;
    }
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Finish")
        {
            FindObjectOfType<SceneController>().NextLevel();
        }
        if (collision.gameObject.tag == "Water")
        {
            FindObjectOfType<SceneController>().RestartLevel();
        }
    }

    public void Sleep()
    {
        FindObjectOfType<AudioManager>().PlaySound("Sleep");
        FindObjectOfType<AudioManager>().PlaySound("Yawn");
        gameObject.GetComponent<PlayerController2>().enabled = false;
        if (!gameObject.name.Contains("Player_inertia"))
        {
            //gameObject.transform.GetComponent<Rigidbody2D>().bodyType = RigidbodyType2D.Kinematic;
            //rb.constraints = RigidbodyConstraints2D.FreezePositionX | RigidbodyConstraints2D.FreezeRotation;
            rb.constraints = RigidbodyConstraints2D.FreezeRotation;
            rb.mass = 999;
            rb.gravityScale = 4;
        }
    }
}
