﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderTimer : MonoBehaviour
{
    public int maxtimer = 10;
    public int timer = 5;
    public Slider slider;
    public float invoke = 1f;

    bool isOn = false;
    // [HideInInspector] private SceneController scenecontroller;

    private void Start()
    {
        slider.maxValue = maxtimer;
        slider.value = timer;
    }
    private void Update()
    {
        if (isOn)
        {
            slider.value -= Time.deltaTime;
            TimeEnds();
        }
    }

    public void TimeEnds()
    {
        if (slider.value <= 0)
        {
            FindObjectOfType<GameManager>().NextPlayer();

            isOn = false;
            slider.value = timer;
        }
    }

    public void StartCount()
    {
        isOn = true;
    }

    public void StopCount()
    {
        isOn = false;
    }


}
