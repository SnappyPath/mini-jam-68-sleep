﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject startPoint;
    int currentPlayer;

    public PlayerController Player_1;
    public PlayerController2 Player_2;
    public PlayerController_Inertia Player_3;

    bool isSelected = false;

    bool isOn = false;
    bool[] banned = new bool[3];

    void Start()
    {
        Player_1.gameObject.SetActive(false);
        Player_2.gameObject.SetActive(false);
        Player_3.gameObject.SetActive(false);
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            FindObjectOfType<SceneController>().RestartLevel();
        }

        if (!isOn && isSelected && (Input.GetKey(KeyCode.A)
                                || Input.GetKey(KeyCode.D)
                                || Input.GetKey(KeyCode.W)
                                || Input.GetButtonDown("Jump")))
        {
            isOn = true;
            FindObjectOfType<SliderTimer>().StartCount();
            banned[currentPlayer] = true;           
        }
    }

    public void NextPlayer()
    {
        if (currentPlayer == 0)
        {
            Player_1.Sleep();

            isOn = false;
            isSelected = false;
        }
        else if (currentPlayer == 1)
        {
            Player_2.Sleep();

            isOn = false;
            isSelected = false;
        }
        else if (currentPlayer == 2)
        {
            Player_3.Sleep();

            isOn = false;
            isSelected = false;
        }
        if (banned[0] && banned[1] && banned[2])
        {
            FindObjectOfType<AudioManager>().PlaySound("Lose");
            Invoke("CallRestartScene", 1f);
        }
    }

    public void btn1_Click()
    {
        if (isOn || banned[0])
            return;

        if (!isSelected)
        {
            Player_1.gameObject.transform.position = startPoint.transform.position;
            Player_1.gameObject.SetActive(true);

            isSelected = true;
            currentPlayer = 0;
        }
        else
        {
            if (!banned[1])
                Player_2.gameObject.SetActive(false);
            if (!banned[2])
                Player_3.gameObject.SetActive(false);

            Player_1.gameObject.transform.position = startPoint.transform.position;
            Player_1.gameObject.SetActive(true);
            currentPlayer = 0;
        }
    }

    public void btn2_Click()
    {
        if (isOn || banned[1])
            return;

        if (!isSelected)
        {
            Player_2.gameObject.transform.position = startPoint.transform.position;
            Player_2.gameObject.SetActive(true);

            isSelected = true;
            currentPlayer = 1;
        }
        else
        {
            if (!banned[0])
                Player_1.gameObject.SetActive(false);
            if (!banned[2])
                Player_3.gameObject.SetActive(false);

            Player_2.gameObject.transform.position = startPoint.transform.position;
            Player_2.gameObject.SetActive(true);
            currentPlayer = 1;
        }
    }

    public void btn3_Click()
    {
        if (isOn || banned[2])
            return;

        if (!isSelected)
        {
            Player_3.gameObject.transform.position = startPoint.transform.position;
            Player_3.gameObject.SetActive(true);

            isSelected = true;
            currentPlayer = 2;
        }
        else
        {
            if (!banned[0])
                Player_1.gameObject.SetActive(false);
            if (!banned[1])
                Player_2.gameObject.SetActive(false);

            Player_3.gameObject.transform.position = startPoint.transform.position;
            Player_3.gameObject.SetActive(true);
            currentPlayer = 2;
        }

    }
    public void CallRestartScene()
    {
        FindObjectOfType<SceneController>().RestartLevel();
    }

    public void RemoveKey()
     {
          if (GameObject.FindGameObjectsWithTag("Key").Length == 1)
            OpenGate();
    }

    void OpenGate()
    {
        Destroy(GameObject.Find("Gate"));
    }



    //public void btn2_Click()
    //{
    //    if (!isActive_1 && !isActive_2 && !isActive_3)
    //    {
    //        Player_2.gameObject.transform.position = startPoint.transform.position;
    //        Player_2.gameObject.SetActive(true);
    //        isActive_2 = true;
    //    }
    //    else if (isActive_2)
    //        return;
    //    else if (isActive_1)
    //    {
    //        Player_1.gameObject.SetActive(false);
    //        isActive_1 = false;

    //        Player_2.gameObject.transform.position = Player_1.gameObject.transform.position;
    //        Player_2.gameObject.SetActive(true);
    //        isActive_2 = true;
    //    }
    //    else if (isActive_3)
    //    {
    //        Player_3.gameObject.SetActive(false);
    //        isActive_3 = false;

    //        Player_2.gameObject.transform.position = Player_3.gameObject.transform.position;
    //        Player_2.gameObject.SetActive(true);
    //        isActive_2 = true;
    //    }
    //}
}
